public class AutoEntreprise extends Entreprise implements IReglerImpots{
    public AutoEntreprise(String siret, String denomination) {
        super(siret, denomination);
        this.impots = 0.25;
    }

    public double payerImpots(double CA) {
        return (double)(CA * this.getImpots());
    }
}
