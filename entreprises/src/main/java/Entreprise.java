abstract class Entreprise {
     protected String siret;
     protected String denomination;
     protected double impots;

     public Entreprise(String siret, String denomination) {
          this.siret = siret;
          this.denomination = denomination;
     }

     public String getSiret() {
          return siret;
     }

     public void setSiret(String siret) {
          this.siret = siret;
     }

     public String getDenomination() {
          return denomination;
     }

     public void setDenomination(String denomination) {
          this.denomination = denomination;
     }

     public double getImpots() {
          return (double)impots;
     }

     public void setImpots(double impots) {
          this.impots = impots;
     }
}
