public class SAS extends Entreprise implements IReglerImpots {
    protected String siegeSocial;

    public SAS(String siret, String denomination, String siegeSocial) {
        super(siret, denomination);
        this.impots = 0.33;
        this.siegeSocial = siegeSocial;
    }

    public double payerImpots(double CA) {
        return (double)(CA * this.getImpots());
    }
}
