import org.junit.Test;

import static org.junit.Assert.*;

public class AutoEntrepriseTest {

    private AutoEntreprise go = new AutoEntreprise("0123456789ABCD", "OK");

    @Test
    public void getImpots() {
        assertEquals(0.25, go.getImpots(), 0.0);
    }

    @Test
    public void getSiret() {
        assertEquals("0123456789ABCD", go.getSiret());
    }

    @Test
    public void getDenomination() {
        assertEquals("OK", go.getDenomination());
    }


    @Test
    public void payerImpots() {
        assertEquals(50.0, go.payerImpots(200.0), 0.0);
    }
}