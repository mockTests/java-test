import org.junit.Test;

import static org.junit.Assert.*;

public class SASTest {

    private SAS go = new SAS("ABCD0123456789", "Back", "Paris");

    @Test
    public void getImpots() {
        assertEquals(0.33, go.getImpots(), 0.0);
    }

    @Test
    public void payerImpots() {
        assertEquals(66.0, go.payerImpots(200), 0.0);
    }

    @Test
    public void getSiret() {
        assertEquals("ABCD0123456789", go.getSiret());
    }
}
